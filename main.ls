require! {express, \node-mattermost, irc, \body-parser}

mattermost = new nodeMattermost "https://myrocket.pw/hooks/mn7n4aeiyjgwxgnd7misjaf3gc"
client = new irc.Client "irc.snoonet.org", "CJOM", do
  port: 6697, secure: true, self-signed: true, channels: ['#CelebJOMaterial']
  flood-protection: true

app = express!

app.use body-parser!

app.post '/irc', (req, res) !->
  #console.log req.body
  client.say '#CelebJOMaterial', "<#{req.body.user_name}> #{req.body.text}"
  res.json {}

client.add-listener 'message', (sender, recipient, text, message) !->
  mattermost.send {text, username: "#{sender}-irc", icon_url: "http://i.imgur.com/aR3xU6g.png"}

client.add-listener 'error', () !->
  console.log "IRC error"

app.listen 6969, -> console.log "Listening on 6969!"
